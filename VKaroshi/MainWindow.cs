using System;
using Gtk;
using VKaroshi;

public partial class MainWindow: Gtk.Window
{	
	private GameUIGtkSharp _game;

	public MainWindow (): base (Gtk.WindowType.Toplevel)
	{
		Build ();
		_game = new GameUIGtkSharp (this);
		radiobutton5.Activate ();
		_game.setType(5, 11, 0.16);
	}
	
	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}

	public Gtk.TextView log { get { return textview1; } }
	public Gtk.Label balance { get { return label2; } }
	public Gtk.Table betButtonsTable { get { return table1; } }
	public Gtk.Table gameTable { get { return table2; } }
	public Gtk.ComboBox mapSize { get { return combobox3; } }
	public Gtk.Button mainButton { get { return button15; } }

	protected void OnButton15Clicked (object sender, EventArgs e)
	{
		_game.mainButtonClick ();
	}

	protected void OnRadiobutton1Toggled (object sender, EventArgs e)
	{
		if (radiobutton1.Active) {
			_game.setBet(0.01);
		}
	}

	protected void OnRadiobutton2Toggled (object sender, EventArgs e)
	{
		if (radiobutton2.Active) {
			_game.setBet(0.02);
		}
	}

	protected void OnRadiobutton3Toggled (object sender, EventArgs e)
	{
		if (radiobutton3.Active) {
			_game.setBet(0.04);
		}
	}

	protected void OnRadiobutton4Toggled (object sender, EventArgs e)
	{
		if (radiobutton4.Active) {
			_game.setBet(0.08);
		}
	}

	protected void OnRadiobutton5Toggled (object sender, EventArgs e)
	{
		if (radiobutton5.Active) {
			_game.setBet(0.16);
		}
	}

	protected void OnRadiobutton6Toggled (object sender, EventArgs e)
	{
		if (radiobutton6.Active) {
			_game.setBet(0.32);
		}
	}

	protected void OnRadiobutton7Toggled (object sender, EventArgs e)
	{
		if (radiobutton7.Active) {
			_game.setBet(0.64);
		}
	}

	protected void OnRadiobutton8Toggled (object sender, EventArgs e)
	{
		if (radiobutton8.Active) {
			_game.setBet(1.00);
		}
	}

	protected void OnRadiobutton9Toggled (object sender, EventArgs e)
	{
		if (radiobutton9.Active) {
			_game.setBet(2.00);
		}
	}

	protected void OnCombobox3Changed (object sender, EventArgs e)
	{
		switch (combobox3.Active) {
			case 1:		_game.setSize(3,6); break;
			default:	_game.setSize(5,11); break;
		}
	}
}
