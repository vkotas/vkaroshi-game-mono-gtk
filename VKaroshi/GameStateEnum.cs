using System;

namespace VKaroshi
{
	public enum GameStateEnum
	{
		NOT_STARTED,
		RUNNING,
		LOST,
		WIN
	}
}

