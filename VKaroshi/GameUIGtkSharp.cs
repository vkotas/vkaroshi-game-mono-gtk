using System;
using Gtk;
using System.Collections.Generic;

namespace VKaroshi
{
	public class GameUIGtkSharp : Game
	{
		private GameType _type;
		private MainWindow _win;
		private Gtk.Button[,] _gameButtons;
		private Dictionary <int, int[]> _gameButtonHash = new Dictionary<int, int[]>();
		//private Icon _icon_ok;
		//private Icon _icon_bomb;
		//private Icon _icon_fail;
		//private Icon _icon_coins;

		public GameUIGtkSharp(MainWindow win) {
			//this.icon_ok = new ImageIcon(getClass().getResource("/vkaroshi/resources/icons/step.png"));
			//this.icon_bomb = new ImageIcon(getClass().getResource("/vkaroshi/resources/icons/bomb.png"));
			//this.icon_fail = new ImageIcon(getClass().getResource("/vkaroshi/resources/icons/fail.gif"));            
			//this.icon_coins = new ImageIcon(getClass().getResource("/vkaroshi/resources/icons/coins.png"));            
			_win = win;
			this.initScore(_win.balance);
			this.logAppend(String.Format("Welcome to the VKaroshi game! You have received {0:0.00}€ initial credit.", _score.getScore()));        
		}

		public void mainButtonClick(){
			if(this.isRunning()) {
				this.takeWin();
			}
			else {
				this.startGameplay();
			}        
		}
		
		private void takeWin(){
			double result = this.endGame();
			this.showBombs();
			this.endGameplay(true);
			this.logAppend(String.Format("You won {0:0.00}€.", result));    
		}
		
		private void startGameplay() {
			try {
				this.toggleControls(false);
				this.startGame(_type);
				this.resetGame();        
				this.logAppend(String.Format("Game started. Your bet is {0:0.00}€.", _type.bet));                
				this.setTakelabel();
				this.enableRow();            
			}
			catch (ArgumentException ex) {
				Gtk.MessageDialog error = new Gtk.MessageDialog(_win, Gtk.DialogFlags.Modal, Gtk.MessageType.Error, Gtk.ButtonsType.Close, ex.ToString());
				error.Title="Error";
				error.Run ();
				error.Destroy ();
				this.endGameplay(false);
			}    
		}
		
		private void endGameplay(bool win){
			if(win) { 
				_win.ModifyBg (StateType.Normal, new Gdk.Color(0, 255, 0)); 
			} else { 
				_win.ModifyBg (StateType.Normal, new Gdk.Color(255, 0, 0)); 
			}
			this.disableGameButtons();
			this.setTakelabel(true);  
			this.toggleControls(true);            
		}
		
		private void enableRow(){
			for(int i=_currentRow; i<=_type.rows; i++){
				for(int j=1; j<=_type.cols; j++) {
					if(i==_currentRow) { 
						_gameButtons[i,j].Sensitive = true;
					} else { 
						_gameButtons[i,j].Sensitive = false;
					}
				}
			}
		}
		
		private void disableGameButtons(){
			for(int i=1; i<=_type.rows; i++){
				for(int j=1; j<=_type.cols; j++) {
					_gameButtons[i,j].Sensitive = false;
				}
			}
		}
		
		private void toggleControls(bool b){
			_win.betButtonsTable.Sensitive = b;
			_win.mapSize.Sensitive = b;
		}
		
		private void setTakelabel(bool revert){
			String t = "";
			//Icon i;
			if(revert) { 
				t = "Start game"; 
				//i = this.icon_bomb;
			}
			else { 
				t = String.Format("Take {0:0.00}€", this.getCurrentWin()); 
				//i = this.icon_coins; 
			}
			//jButtonStart.setText(t);
			//jButtonStart.setIcon(i);
			_win.mainButton.Label = t;
		}
		
		private void setTakelabel() {
			this.setTakelabel(false);
		}
		
		public void setType(int cols, int rows, double bet){
			_type = new GameType();
			_type.bet = bet;
			setSize(cols, rows);
		}
		
		public void resetGame(){
			this.setSize(_type.cols, _type.rows);
			this.setBombs();
			_win.ModifyBg (StateType.Normal); 
		}
		
		
		public void setSize(int cols, int rows) {
			_gameButtons = new Gtk.Button [rows + 1, cols + 1];
			_gameButtonHash = new Dictionary<int, int[]> ();

			foreach (Gtk.Widget w in _win.gameTable.Children) {
				_win.gameTable.Remove(w);
			}

			_win.gameTable.NColumns  = (uint) cols + 1;
			_win.gameTable.NRows  = (uint) rows;
			_type.setSize (cols, rows);

			for (int i=1; i <= rows; i++) {
				for(int j=1; j<=cols; j++){
					_gameButtons[i,j] = new global::Gtk.Button ();
					_gameButtons[i,j].CanFocus = true;
					_gameButtons[i,j].Sensitive = false;
					_gameButtons[i,j].Name = "gameButton"+i+"_"+j;
					_gameButtons[i,j].UseUnderline = true;
					_gameButtons[i,j].Label = global::Mono.Unix.Catalog.GetString ("");

					_gameButtonHash.Add( _gameButtons[i,j].GetHashCode(), new int[2] { i, j });
					_gameButtons[i,j].Clicked +=  (sender, e) => gameButtonClick(sender.GetHashCode());

					_win.gameTable.Add(_gameButtons[i,j]);
					global::Gtk.Table.TableChild w3 = ((global::Gtk.Table.TableChild)(_win.gameTable [_gameButtons[i,j]]));
					w3.TopAttach = ((uint)(i));
					w3.BottomAttach = ((uint)(i+1));
					w3.LeftAttach = ((uint)(j));
					w3.RightAttach = ((uint)(j+1));
					w3.YOptions = ((global::Gtk.AttachOptions)(4));
				}
				Gtk.Label label = new global::Gtk.Label (String.Format("x {0:0.00}", _type.getRates(i)));
				label.Justify = Gtk.Justification.Left;
				_win.gameTable.Add(label);
				global::Gtk.Table.TableChild w2 = ((global::Gtk.Table.TableChild)(_win.gameTable [label]));
				w2.YOptions = ((global::Gtk.AttachOptions)(4));
				w2.LeftAttach = ((uint)(cols+1));
				w2.RightAttach = ((uint)(cols+2));
				w2.TopAttach = ((uint)(i));
				w2.BottomAttach = ((uint)(i+1));
			}

			_win.gameTable.Show ();
			_win.gameTable.ShowAll ();
		}
		
		protected void gameButtonClick(int j, int i) {
			if(!this.isRunning()){
				throw new ArgumentException("Game is not started");
			}
			
			if(i ==_currentRow) {
				this.showBomb();
				_gameButtons[_currentRow, j].Label = "OK";
				//this.jButtonGame[this.currentRow][j].setIcon(icon_ok);
				this.gameButtonClickGoodRow(j);
			}
		}

		private void gameButtonClick(int hash) {
			int[]value;
			_gameButtonHash.TryGetValue (hash, out value);
			gameButtonClick (value[1], value[0]);
		}

		private void gameButtonClickGoodRow(int j){
			if(this.playRow(j)){
				if(_currentRow==_type.rows) { nextRound(); mainButtonClick(); }
				else {
					this.nextRound();            
					this.enableRow();
					this.setTakelabel(); 
				}
			}
			else {
				this.logAppend(String.Format("Bang! You lost {0:0.00}€.", _type.bet));
				//this.jButtonGame[this.currentRow][j].setIcon(icon_fail);
				_gameButtons[_currentRow, j].Label = "Bang!";
				this.showBombs(_currentRow);
				this.endGameplay(false);
			}        
		}
		
		private void showBomb(int row){
			_gameButtons[row, this.getBombs(row)].Label = "Bomb";
			//this.jButtonGame[row][this.getBombs(row)].setIcon(icon_bomb);
		}
		
		private void showBomb(){
			this.showBomb(_currentRow);
		}
		
		private void showBombs(int e){
			for(int i=1;i<=_type.rows; i++) {
				if(i!=e) { showBomb(i); }
			}
		}
		
		private void showBombs(){
			showBombs(0);
		}
		
		public void setBet(double bet) {
			for (int i = 1; i < _gameButtons.GetLength(0); i++)
			{
				for(int j = 1; j <= _type.cols; j++) {
					_gameButtons[i, j].Label = "";
				}
			}

			_type.bet = bet;
		}
		
		private void logAppend(String t){
			DateTime d = DateTime.Now;
			Gtk.TextIter iter = _win.log.Buffer.GetIterAtLine (0);
			_win.log.Buffer.Insert(iter, String.Format("{0:dd.MM.yy H:mm:ss} - {1}\n", d, t));    
		}	}
}

