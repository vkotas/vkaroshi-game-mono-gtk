using System;

namespace VKaroshi
{
	public class GameType
	{
		private int _cols = 0;
		private int _rows = 0;
		private double _bet = 0;
		private double[] _rates;

		public int cols
		{
			get { return _cols; }
		}

		public int rows
		{
			get { return _rows; }
		}

		public double[] rates
		{
			get { return _rates; }
		}

		public double getRates(int i) {
			return _rates[i];
		}

		public double bet
		{
			get { return _bet; }
			set { _bet = value; }
		}

		public GameType (int cols, int rows, double bet) {
			_cols = cols;
			_rows = rows;
			_bet = bet;
		}
		
		public GameType () {}
		
		public void setSize(int cols, int rows) {
			_cols = cols;
			_rows = rows;        
			this.calcRates(cols, rows);    
		}
		
		private void calcRates(int cols, int rows){
			_rates = new double[rows+1];
			for(int i=1; i <= rows; i++){
				this.rates[i] = Math.Pow((1+(1/(double)cols)), i);
				
			}
		}
	}
}

