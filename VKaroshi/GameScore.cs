using System;

namespace VKaroshi
{
	public interface GameScore
	{
		double getScore();
		void setScore(double s);
		bool adjustScore(double ch); 
	}
}

