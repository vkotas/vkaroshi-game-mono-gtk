using System;

namespace VKaroshi
{
	public class Game
	{
		public Game ()
		{
		}

		protected GameScore _score;
		private GameType _type;
		private GameStateEnum _state = GameStateEnum.NOT_STARTED;
		protected int _currentRow = 0;
		private int[] _bombs;
		
		public void initScore(Gtk.Label display){
			_score = new GameScoreVariable(display);
		}
		
		public void startGame(GameType t){
			if(_score.getScore()<0.01) {
				throw new ArgumentException("You don't have enough money to play.");
			}
			else if(t.bet > _score.getScore()) {
				throw new ArgumentException("Your bet cannot be higher than your balance.");
			}
			_type = t;
			setBombs();
			_state = GameStateEnum.RUNNING;
			_currentRow = 1;
			_score.adjustScore(-_type.bet);
		}
		
		public bool playRow(int i){
			if(_bombs[_currentRow]==i){
				_state = GameStateEnum.LOST;
				return false;
			} {
				return true;
			}
		}
		
		public void nextRound() {
			_currentRow++;    
		}
		
		public double getCurrentWin(){
			if(_currentRow<2) { return _type.bet; }
			else { return _type.bet*_type.rates[_currentRow-1];}
		}
		
		
		public double endGame(){
			if(_state==GameStateEnum.RUNNING) {
				_state = GameStateEnum.WIN;
				double win = getCurrentWin();
				_score.adjustScore(win);
				return win;
			}
			return 0;
		}
		
		protected void setBombs(){
			int max = _type.cols+1;
			Random random = new Random();
			_bombs = new int[_type.rows+1];
			for(int i =1; i <= _bombs.Length-1; i++){
				_bombs[i] = random.Next(1, max); 
			}
		}
		
		protected int[] getBombs(){
			return _bombs;
		}
		
		protected int getBombs(int i){
			return _bombs[i];
		}
		
		public GameStateEnum bet
		{
			get { return _state; }
			set { _state = value; }
		}
		
		public bool isRunning(){
			if(_state==GameStateEnum.RUNNING) { return true; }
			return false;
		}
		
		public bool isWin(){
			if(_state==GameStateEnum.WIN) { return true; }
			return false;
		}    


	}
}

