using System;

namespace VKaroshi
{
	public class GameScoreVariable : GameScore
	{
		public GameScoreVariable ()
		{
		}

		private double _score = 0;
		private Gtk.Label _display;
		
		public GameScoreVariable(Gtk.Label display){
			_score = 10;
			_display = display; 
			setDisplay();     
		}
		
		public GameScoreVariable(double d, Gtk.Label display){
			_score = d;
			_display = display; 
			setDisplay();       
		}

		public double getScore(){
			return _score;
		}
		
		public void setScore(double s){
			_score = s;
			setDisplay();        
		}
		public bool adjustScore(double ch){
			_score += ch;
			setDisplay();
			if(_score < 0) { return false; }
			else { return true; }
		}
		
		private void setDisplay(){
			_display.LabelProp = String.Format("{0:0.00}", _score);        
		}

	}
}

